<?php

require_once "base.php";
require_once "Author.php";
require_once "Book.php";

function getAuthorsArray()
{
    $authors = [];

    $conn = getConnection();
    $stmt = $conn->prepare("select * from authors");
    $stmt->execute();

    foreach ($stmt as $author) {
        array_push($authors,
            new Author(
                urldecode($author["firstName"]),
                urldecode($author["lastName"]),
                urldecode($author["grade"]),
                $author["id"]));
    }
    return $authors;
}



function getBooksArray(){
    $books = [];

    $conn = getConnection();
    $stmt = $conn->prepare("select * from books");
    $stmt->execute();

    foreach ($stmt as $book) {
        $id = $book["id"];
        $title = urldecode($book["title"]);
        $grade = $book["grade"];
        $author = $book["author"];
        $isRead = $book["isRead"];

        array_push($books, new Book($title, $grade, $isRead, $author, $id));
    }
    return $books;
}

function getAuthorByID($authorID)
{
    $grade = "0";
    $authorFirstName = "";
    $authorLastName = "";

    $conn = getConnection();
    $bookAuthor = $conn->prepare("select firstname, lastname, grade from authors where id='$authorID'");
    $bookAuthor->execute();

    foreach ($bookAuthor as $n) {
        $grade = $n["grade"];
        $authorFirstName = isset($n["firstname"]) ? $n["firstname"] : " ";
        $authorLastName = isset($n["lastname"]) ? $n["lastname"] : " ";
    }

    return new Author($authorFirstName, $authorLastName, $grade, $authorID);
}

function getBookByID($id){
    $conn = getConnection();
    $stmt = $conn->prepare("select title, author, grade, isRead from books where id='$id'");
    $stmt->execute();

    foreach ($stmt as $book) {
        $title = isset($book["title"]) ? urldecode($book["title"]) : "";
        $author1ID = isset($book["author"]) ? urldecode($book["author"]) : "";
        $grade = isset($book["grade"]) ? urldecode($book["grade"]) : "0";
        $isRead = isset($book["isRead"]) ? urldecode($book["isRead"]) : 0;
    }
    return new Book($title, $grade, $isRead, $author1ID, $id);
}

function getMessage($add, $obj){
    $message = "";

    if ($add == 1) {$message = $obj." lisanud.";}
    elseif ($add == 2) {$message = $obj." muudanud.";}
    elseif ($add == 3) {$message = $obj." kustutatud.";}

    return $message;
}

function getBookBy_POST($POST){
    $id = isset($POST["id"]) ? $POST["id"] : "";
    $title = isset($POST["title"]) ? $POST["title"] : "";
    $author1ID = isset($POST["author1"]) ? $POST["author1"] : 0;
    $grade = isset($POST["grade"]) ? $POST["grade"] : "0";
    $isRead = isset($POST["isRead"]) ? $POST["isRead"] : 0;

    return new Book($title, $grade, $isRead, $author1ID, $id);

}

function getAuthorBy_POST($POST){
    $id = $POST["id"];
    $firstName = $POST["firstName"];
    $lastName = $POST["lastName"];
    $grade = $POST["grade"];

    return new Author($firstName, $lastName, $grade, $id);


}