<?php
require_once 'vendor/tpl.php';
require_once 'Request.php';
require_once "base.php";
require_once "Author.php";
require_once "Book.php";
require_once "scripts.php";

$request = new Request($_REQUEST);

/*print($request);
print_r($_POST);
print_r($_GET);*/

$data = [];
$cmd = $request->param('cmd')
    ? $request->param('cmd')
    : 'show_book_list';


if ($cmd === 'show_book_form') {
    $book = getBookBy_POST($_POST);

    if ($_SERVER["REQUEST_METHOD"] === "POST" and !(strlen($book->title) < 3 or strlen($book->title) > 23)) {
        $book->add();
        header("Location: /index.php?cmd=show_book_list&add=1");
        exit();
    } else if ($_SERVER["REQUEST_METHOD"] === "GET") {
        $isError = false;
    }

    $data = [
        "error" => "Pealkirja pikkus on 3 kuni 23 märki",
        "isError" => isset($isError) ? $isError : true,
        "book" => $book,
        "authors" => getAuthorsArray(),
        'template' => 'book-add.html',
        'cmd' => 'show_book_form',
    ];

} else if ($cmd === 'show_author_form') {
    $author = getAuthorBy_POST($_POST);

    if ($_SERVER["REQUEST_METHOD"] === "POST" and
        !(strlen($author->firstName) < 1 or strlen($author->firstName) > 21 or strlen($author->lastName) < 2 or strlen($author->lastName) > 22)) {
        $author->add();
        header("Location: /index.php?cmd=show_author_list&add=1");
        exit();
    } else if ($_SERVER["REQUEST_METHOD"] === "GET") {
        $isError = false;
    }
    $data = [
        "error" => "Eesnime pikkus on 1 kuni 21 märki
                ja perekonnanime pikkus on 2 kuni 22 märki!",
        "isError" => isset($isError) ? $isError : true,
        "author" => $author,
        'template' => 'author-add.html',
        'cmd' => 'show_author_form',
    ];

} else if ($cmd === 'show_book_list') {
    $books = getBooksArray();
    $add = isset($_GET["add"]) ? $_GET["add"] : "";
    $message = getMessage($add, "Raamat");
    $data = [
        "books" => $books,
        "message" => $message,
        'template' => 'book-list.html',
        'cmd' => 'show_book_list',
    ];

} else if ($cmd === 'show_author_list') {
    $authors = getAuthorsArray();
    $add = isset($_GET["add"]) ? $_GET["add"] : "";
    $message = $message = getMessage($add, "Autor");
    $data = [
        'message' => $message,
        'authors' => $authors,
        'template' => 'author-list.html',
        'cmd' => 'show_author_list',
    ];

} else if ($cmd === 'show_author_edit_form') {
    $isError = false;

    if ($_SERVER["REQUEST_METHOD"] === "GET") {
        $isError = false;
        $id = intval($_GET["id"]);
        $author = getAuthorByID($id);
    } else {
        $delete = isset($_POST["deleteButton"]) ? $_POST["deleteButton"] : "";
        $submit = isset($_POST["submitButton"]) ? $_POST["submitButton"] : "";

        $author = getAuthorBy_POST($_POST);
    }

    if ($_SERVER["REQUEST_METHOD"] === "POST" and $delete == "kustuda") {
        $author->delete();
        header("Location: /index.php?cmd=show_author_list&add=3");
        exit();

    } else if ($_SERVER["REQUEST_METHOD"] === "POST" and
        !(strlen($author->firstName) < 1 or strlen($author->firstName) > 21
            or strlen($author->lastName) < 2 or strlen($author->lastName) > 22)) {
        $author->update();
        header("Location: /index.php?cmd=show_author_list&add=2");
        exit();
    }

    $data = [
        "error" => "Eesnime pikkus on 1 kuni 21 märki ja perekonnanime pikkus on 2 kuni 22 märki!",
        "isError" => isset($isError) ? $isError : true,
        "author" => $author,
        'template' => 'author-edit.html',
        'cmd' => 'show_author_edit_form',
    ];


} else if ($cmd === 'show_book_edit_form') {

    if ($_SERVER["REQUEST_METHOD"] === "GET") {
        $isError = false;
        $id = $_GET["id"];
        $book = getBookByID($id);
    } else {
        $book = getBookBy_POST($_POST);
    }

    if ($_SERVER["REQUEST_METHOD"] === "POST" and $_POST["deleteButton"] == "kustuda") {
        $book->delete();
        header("Location: /index.php?cmd=show_book_list&add=3");
        exit();

    } else if ($_SERVER["REQUEST_METHOD"] === "POST" and $_POST["submitButton"] == "salvesta" and !(strlen($book->title) < 3 or strlen($book->title) > 23)) {
        $book->update();
        header("Location: /index.php?cmd=show_book_list&add=2");
        exit();
    }

    $data = [
        "error" => "Pealkirja pikkus on 3 kuni 23 märki",
        "isError" => isset($isError) ? $isError : true,
        "book" => $book,
        "authors" => getAuthorsArray(),
        'template' => 'book-edit.html',
        'cmd' => 'show_book_edit_form'
    ];
}
print renderTemplate($data['template'], $data);
