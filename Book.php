<?php
require_once "base.php";

class Book
{
    public $id;
    public $title;
    public $grade;
    public $isRead;
    public $authorID;

    public function __construct($title, $grade, $isRead, $authorID, $id = 0)
    {
        $this->id = $id;
        $this->title = $title;
        $this->grade = $grade;
        $this->isRead = $isRead;
        $this->authorID = $authorID;

    }

    public function getFullAuthorName()
    {
        $conn = getConnection();
        $name = $conn->prepare("select firstname, lastname from authors where id=$this->authorID");
        $name->execute();
        $authorFullName = "";
        foreach ($name as $n) {
            $authorFirstName = urldecode($n["firstname"]);
            $authorLastName = isset($n["lastname"]) ? urldecode($n["lastname"]) : " ";
            $authorFullName = $authorFirstName . " " . $authorLastName;
        }
        return $authorFullName;
    }

    public function add()
    {
        $title = urlencode($this->title);
        $grade = $this->grade;
        $isRead = $this->isRead;
        $authorID = $this->authorID;

        $conn = getConnection();
        $comand = "insert into books (title, grade, isRead, author) values ('$title', '$grade', '$isRead', $authorID)";
        $stmt = $conn->prepare($comand);
        $stmt->execute();

    }

    public function delete()
    {
        $conn = getConnection();
        $stmt = $conn->prepare("delete from books where id = '$this->id'");
        $stmt->execute();
    }

    public function update()
    {
        $title = urlencode($this->title);
        $grade = $this->grade;
        $isRead = $this->isRead;
        $author1ID = $this->authorID;

        $conn = getConnection();
        $stmt = $conn->prepare("update books set title = '$title', grade = '$grade', isRead = '$isRead', author = '$author1ID' where id = '$this->id'");
        $stmt->execute();
    }
}