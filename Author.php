<?php
require_once "base.php";


class Author
{
    public $id;
    public $firstName;
    public $lastName;
    public $grade;

    public function __construct($firstName, $lastName, $grade, $id=0) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->grade = $grade;
    }

    public function getFullName(){
        return $this->firstName." ".$this->lastName;
    }

    public function add(){
        $firstName = urlencode($this->firstName);
        $lastName = urlencode($this->lastName);
        $grade = urlencode($this->grade);

        $conn = getConnection();
        $stmt = $conn->prepare(
            "insert into authors (firstName, lastName, grade) values ('$firstName', '$lastName', '$grade')");
        $stmt->execute();
    }

    public function delete(){
        $conn = getConnection();
        $stmt = $conn->prepare("delete from authors where id = '$this->id'");
        $stmt->execute();
    }

    public function update(){
        $conn = getConnection();

        $firstName = urlencode($this->firstName);
        $lastName = urlencode($this->lastName);
        $grade = urlencode($this->grade);
        $stmt = $conn->prepare("update authors set firstName = '$firstName', lastName = '$lastName', grade = '$grade' where id = '$this->id'");
        $stmt->execute();
    }

}